/**
 *  A task SHOULD ALWAYS complete successfully, which means any necessary error-handling
 *  must be contained within the task.  Any task failures will be 'dropped on the floor'.
 */
export type PQRunnable = () => Promise<void>;

export interface PQJob {
    id: string;
    priority: number;
    run: PQRunnable;
}

export interface PQStatus {
    size: number;

    /** This can be used to determine the probability of any given job being selected based on priority */
    totalPriority: number;

    /** A Measurement of the overhead each time the queue executes a job */
    avgOverheadMs: number;
    runningJobs: number;
}

type JobWithOptionalId = Omit<PQJob, 'id'> & Partial<Pick<PQJob, 'id'>>;
type AddTaskArgs = [number, PQRunnable] | [JobWithOptionalId];
type JobId = string;

export class PPQQ {

    private jobsById = new Map<JobId, PQJob>();
    private jobsBySymbol = new Map<symbol, PQJob>();
    private symbols = new Map<JobId, symbol>();
    private queue: symbol[] = [];

    private overheadTotal: number[] = [];

    private runningJobs = 0;

    constructor(
        private overheadStatLength: number = 1000,
        private random: () => number = Math.random,
    ) { }

    addJob(priority: number, job: PQRunnable): string
    addJob(job: JobWithOptionalId): string
    addJob(...args: AddTaskArgs): string {
        let job: PQJob = args.length === 2
            ? { priority: args[0], run: args[1], id: randomId() }
            : args[0] as any;

        job.id = job.id || randomId();

        this.jobsById.set(job.id, job);

        const jobSymbol = Symbol(job.id);
        this.symbols.set(job.id, jobSymbol);
        this.jobsBySymbol.set(jobSymbol, job);
        this.queue.push(...Array.from<symbol>({ length: job.priority }).fill(jobSymbol));

        return job.id;
    }

    removeJob(id: string) {
        const jobSymbol = this.symbols.get(id);
        if (!jobSymbol) return;

        this.jobsById.delete(id);
        this.jobsBySymbol.delete(jobSymbol);
        this.symbols.delete(id);
        this.queue = this.queue.filter(s => s !== jobSymbol);
    }

    nextJob(): Promise<void> {
        const start = Date.now();
        const jobSymbol = sample(this.queue, this.random);
        const job = this.jobsBySymbol.get(jobSymbol)!;

        this.removeJob(job.id);
        this.updateOverhead(Date.now() - start);

        this.runningJobs++;
        return job.run().finally(() => this.runningJobs--);
    }

    isEmpty(): boolean {
        return this.queue.length === 0;
    }

    status(): PQStatus {
        return {
            size: Array.from(this.jobsById.keys()).length,
            totalPriority: this.queue.length,
            avgOverheadMs: average(this.overheadTotal),
            runningJobs: this.runningJobs,
        }
    }

    private updateOverhead(val: number) {
        this.overheadTotal.push(val);
        this.overheadTotal = this.overheadTotal.slice(-1 * this.overheadStatLength);
    }
}

function randomId() {
    return Array.from({ length: 3 }).map(() => Math.random().toString(32).slice(2)).join();
}

function average(vals: number[]) {
    return vals.reduce((acc, curr) => acc + curr, 0) / (vals.length || 1);
}

function sample<T>(arr: T[], random: () => number): T {
    const idx = (random() * arr.length) | 0;
    return arr[idx];
}