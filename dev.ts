import { PPQQ } from './lib';
import * as asciichart from 'asciichart';

const TotalJobs = 10000;
const MaxPriority = 100;
const MinPriority = 1;


const priorityRange = MaxPriority - MinPriority;
const randomPriority = () => ((priorityRange * Math.random()) | 0) + MinPriority;

const ppqq = new PPQQ();

Array.from({ length: TotalJobs }).forEach(() => {
    const priority = randomPriority();
    const run = async () => {
        console.log(`Priority: ${priority}`);
    };

    ppqq.addJob({ priority, run });
});

const plots: number[] = [];
(async () => {
    let i = 0;
    while (!ppqq.isEmpty()) {
        if (i % 100 === 0) {
            const status = ppqq.status();
            console.log(status);

            plots.push(status.avgOverheadMs);
        }

        await ppqq.nextJob();
        i++;
    }
})().catch(console.error).finally(() => {
    console.log('Finished')
    if (plots.length) {
        console.log(asciichart.plot(plots, { height: 20 }));
    } else {
        console.log(plots);
    }
});

