# PPQQ - Probabilistic Priority Queue (q)

For a large distributed system, trying to maintain a precise order of operations become untenable; instead, it become more realistic to 'go with the flow'. PPQQ embraces this enevitability and provides a way to schedule any number of jobs where the probability of a job being executed is directly related to its priority.

With this approach, a priority of (1) MAY execute before another job with a priority of (10), although potentially unlikely.  The chance of this happening is dependent on the total size of queue and the aggregate priority of all jobs in the queue.

### Usage

```typescript
import { PPQQ } from '@kiser360/ppqq';

const ppqq = new PPQQ();

const id1 = ppqq.addJob({ priority: 1, run: async () => console.log('Hi Mom'); });
const id2 = ppqq.addJob({ priority: 10, run: async () => console.log('Bananas'); });

await ppqq.nextJob();
ppqq.removeJob(id1); // Not sure which job executed and removed automatically
ppqq.removeJob(id2); //  - it's safe to remove both just in case

const stats = ppqq.status();
assert(stats.size === 0);
assert(ppqq.isEmpty());
```

This lib is just a shower-thought turned into a weekend project.  Perhaps it can be useful to someone or maybe I'll use it in a later project.

### TODO

- [ ] I need a better approach to weighted random selection, currently it introduces too much overhead to be reasonable.  Specifically, iterating arrays is slow.
       - https://richardstartin.github.io/posts/reservoir-sampling
       - https://utopia.duth.gr/~pefraimi/research/data/2007EncOfAlg.pdf